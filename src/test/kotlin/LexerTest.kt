import smck.lexer.Token
import smck.lexer.TokenImpl
import smck.lexer.TokenType
import smck.lexer.TokenType.Error
import smck.lexer.TokenType.Name
import smck.lexer.lex
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class LexerTest {

    private fun assertLexResult(input: String, expected: String) {
        val list = lex(input)
        val actual = list.joinToString() { it.type.code.toString() + it.text }

        assertEquals(expected, actual)
    }

    @Test fun `Verify the basic test environment with the IDE`() {
        assertEquals(4, 2 + 2, "Uh, so sorry...you need a new computer!")
    }

    @Test fun `When given a token that is a single character, verify the correct text value`() {
        TokenType.values().forEach {
            assertEquals(it.code.toString(), TokenImpl(it, it.code.toString(), 0, 0).text, "The code value is wrong!")
        }
    }

    @Test fun `When running lex() on an empty string, verify an empty result list`() {
        assertTrue(lex("").isEmpty(), "lex() produced something from nothing!")
    }

    @Test fun `When running lex() on a single comment line, verify an empty result list`() {
        assertTrue(lex("// xyzzy").isEmpty(), "lex() produced something from nothing!")
    }

    @Test fun `When running lex() on a single character token, verify result`() {
        fun getDiagnostic(snippet: String) = "The $snippet is wrong"

        fun assertToken(input: String, list: List<Token>) {
            assertEquals(1, list.size, getDiagnostic("list size"))
            assertEquals(input, list[0].text, getDiagnostic("token text"))
            assertEquals(1, list[0].position, getDiagnostic("position"))
            assertEquals(1, list[0].line, getDiagnostic("line number"))
        }

        TokenType.values().forEach { type ->
            if (type != Name && type != Error) assertToken(type.code.toString(), lex(type.code.toString()))
        }
    }

    @Test fun `When running lex() on a name token, verify result`() {
        fun getDiagnostic(snippet: String) = "The $snippet is wrong"

        fun assertToken(input: String, list: List<Token>) {
            assertEquals(1, list.size, getDiagnostic("list size"))
            assertEquals(input, list[0].text, getDiagnostic("token type text"))
            assertEquals(1, list[0].position, getDiagnostic("position"))
            assertEquals(1, list[0].line, getDiagnostic("line number"))
        }

        val input = "SomeName"

        assertToken(input, lex(input))
    }

    @Test
    fun `When lex() is presented with an invalid single character input, verify the result`() {
        assertEquals(Error, lex(";")[0].type)
    }

    @Test
    @Throws(Exception::class)
    fun simpleSequence() {
        assertLexResult("{}", "{{, }}")
    }

    @Test
    @Throws(Exception::class)
    fun complexSequence() {
        assertLexResult("FSM:fsm{this}", "|FSM, ::, |fsm, {{, |this, }}")
    }

    @Test
    @Throws(Exception::class)
    fun allTokens() {
        assertLexResult("{}()<>-: name .", "{{, }}, ((, )), <<, >>, --, ::, |name, ?Invalid token detected: 1/15")
    }

    @Test
    @Throws(Exception::class)
    fun multipleLines() {
        assertLexResult("FSM:fsm.\n{bob-.}", "|FSM, ::, |fsm, ?Invalid token detected: 1/8, {{, |bob, --, " +
                "?Invalid token detected: 2/6, }}")
    }
}