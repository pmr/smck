package smck.lexer

enum class TokenType(val code: Char) { OpenBrace('{'), CloseBrace('}'), OpenParen('('), CloseParen(')'),
    OpenAngle('<'), CloseAngle('>'), Dash('-'), Colon(':'), Name('|'), Error('?') }

interface Token {
    val type: TokenType
    val text: String
    val line: Int
    val position: Int
}

class TokenImpl(override val type: TokenType, override val text: String, override val line: Int,
                override val position: Int) : Token

const val InvalidTokenError = "Invalid token detected: "
const val whitespacePattern = """^(\s*).*"""
const val wordPattern = """^(\w+).*"""

fun lex(input: String): List<Token> {
    tailrec fun lexLine(line: String, lineNumber: Int, position: Int, result: MutableList<Token>): List<Token> {
        fun getNextToken(position: Int): Pair<Token, Int> {
            fun getSingleCharacterToken(position: Int): Token {
                val text = line.substring(position, position + 1)
                val list = TokenType.values().filter { it.code.toString() == text }

                return TokenImpl(list[0], text, lineNumber + 1, position + 1)
            }

            fun getNameTokenResult(position: Int): Pair<Token, Int> {
                val tokenType: TokenType = TokenType.Name
                val tokenText = getChars(wordPattern, line, position)
                val nextPosition = position + tokenText.length
                return Pair(TokenImpl(tokenType, tokenText, lineNumber + 1, position + 1), nextPosition)
            }

            fun isSingleCharacterToken(position: Int): Boolean {
                val text = line.substring(position, position + 1)
                val list = TokenType.values().filter { it.code.toString() == text }

                return list.size == 1 && list[0] != TokenType.Error
            }

            fun isNameToken(position: Int) = getChars(wordPattern, line, position).isNotEmpty()

            fun getErrorToken(message: String, position: Int) =
                TokenImpl(TokenType.Error, "$message${lineNumber + 1}/${position + 1}", lineNumber + 1, position + 1)

            val nextPosition = position + getChars(whitespacePattern, line, position).length

            return when {
                isSingleCharacterToken(nextPosition) -> Pair(getSingleCharacterToken(nextPosition), nextPosition + 1)
                isNameToken(nextPosition) -> getNameTokenResult(nextPosition)
                else -> Pair(getErrorToken(InvalidTokenError, nextPosition), nextPosition + 1)
            }
        }

        val (nextToken, nextPosition) = getNextToken(position)

        result.add(nextToken)
        return if (nextPosition >= line.length) result else lexLine(line, lineNumber, nextPosition, result)
    }

    fun skip(line: String) = line.isEmpty() || line.startsWith("//")

    val result: MutableList<Token> = mutableListOf()
    val lines: List<String> = input.split("\n")

    lines.forEachIndexed { lineNumber, line -> if (!skip(line)) lexLine(line, lineNumber, 0, result) }
    return result
}

internal fun getChars(pattern: String, line: String, position: Int): String {
    val regex = pattern.toRegex()
    val matchResult = regex.matchEntire(line.substring(position))
    val groups = matchResult?.groupValues ?: listOf()

    return if (groups.size == 2) groups[1] else ""
}
